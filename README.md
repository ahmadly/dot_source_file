# what is .source file ?

# install 
- install .source file script:  
```bash
curl https://raw.githubusercontent.com/ahmadly/dot_source_file/master/dot_source_file.sh -o  ~/.bash_dot_source_file
```
- load .bash_dot_source_file to ~/.bashrc:  
```bash
echo "source ~/.bash_dot_source_file" >> ~/.bashrc 
```

# usage

